import { state } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(  private route: ActivatedRoute,
    private router: Router) { }

  logEffettuato: boolean = true

  ngOnInit(): void {
  }

  effettuatoLogin(){
    this.router.navigate(['/prenotazione-visita'], {
      state: {
        logEffettuato: this.logEffettuato = true
      }
    })
  }

}
